#include <iostream>
#include <cstdio>
#include <vector>
#include <math.h>
#include <cstdlib>
using namespace std;

int main(){
	unsigned n;
	scanf("%u",&n);

	vector <int> shop;
	for(int i=1;i<=2*n;++i){
		int tmp;
		scanf(" %d",&tmp);
		shop.push_back(tmp);
	}

	unsigned steps=0;
	int place_A=0,place_B=0,tmp=0;
	for(int i=1;i<=n;++i){
		while(shop.at(tmp)!=i) 
			tmp++;
		steps+=(unsigned)(place_A>tmp?place_A-tmp:tmp-place_A);
		place_A=tmp++;
		while(shop.at(tmp)!=i)
			tmp++;
		steps+=(unsigned)(place_B>tmp?place_B-tmp:tmp-place_B);
		place_B=tmp;
		tmp=0;
	}

	printf("%u",steps);

	return 0;
}
